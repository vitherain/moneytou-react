const path = require('path');
const webpack = require('webpack');

const stringifyFn = require('./env/stringify');
let envConfig = require('./env/dev');
envConfig = stringifyFn(envConfig);

module.exports = {
  devtool: 'eval',
  entry: [
    './src/app/index.jsx'
  ],
  output: {
    path: path.join(__dirname, 'src', 'static'),
    filename: 'js/bundle.js'
  },
  plugins: [
    new webpack.DefinePlugin({'process.env': envConfig})
  ],
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          },
          {
            loader: 'sass-loader',
            options: { sourceMap: true }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader'
          }
        ]
      },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$/i,
        use: {
          loader: 'file-loader',
          options: {
            name: './images/[hash].[ext]'
          }
        }
      },
      {
        test: /\.woff(2)?(\?v=\d\.\d\.\d)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 5000,
            mimetype: 'application/font-woff',
            name: './fonts/[hash].[ext]'
          }
        }
      },
      {
        test: /\.ttf(\?v=\d\.\d\.\d)?$|\.eot(\?v=\d\.\d\.\d)?$|\.svg\?v=\d\.\d\.\d$/,
        use: {
          loader: 'file-loader',
          options: {
            name: './fonts/[hash].[ext]'
          }
        }
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env', ["es2015", { "modules": false }], 'react', 'stage-2'],
            plugins: ['transform-class-properties', 'transform-object-rest-spread']
          }
        }
      },
      {
        test: /^https?:\/\//,
        use: 'url-loader'
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    modules: [
      __dirname,
      'node_modules'
    ]
  }
};
