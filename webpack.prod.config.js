const path = require('path');
const webpack = require('webpack');
const config = require('./webpack.config.js');

const stringifyFn = require('./env/stringify');
let envConfig = require('./env/prod');
envConfig = stringifyFn(envConfig);

config.plugins = [
    new webpack.DefinePlugin({'process.env': envConfig}),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.optimize.LimitChunkCountPlugin({maxChunks: 15}),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.LoaderOptionsPlugin({minimize: true}),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      sourceMap: true,
      compress: {
        warnings: true
      }
    })
];

config.devtool = 'source-map';

module.exports = config;