export const getCurrentDate = () => new Date();

export const getCurrentYear = () => new Date().getFullYear();
