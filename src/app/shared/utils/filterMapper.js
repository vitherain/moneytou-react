import * as C from '../config/constants';

export const mapFiltersToRequestParams = function (filtersState = []) {

  let result = {};

  for (let state of filtersState) {
    let operation;

    switch (state.type) {
      case 'text':
        operation = resolveTextKey(state);
        result[operation] = state.value;
    }
  }
  return result;
};

const resolveTextKey = function (state) {
  const caseSensitivePrefix = 'i';
  let operation;

  switch (state.matchType) {
    case C.CONTAINS_TYPE:
      operation = 'like';
      if (!state.caseSensitive) {
        return `${state.path}${C.FILTER_OPERATION_SEPARATOR}${caseSensitivePrefix}${operation}`;
      } else {
        return `${state.path}${C.FILTER_OPERATION_SEPARATOR}${operation}`;
      }
    case C.STARTS_WITH_TYPE:
      operation = 'startswith';
      if (!state.caseSensitive) {
        return `${state.path}${C.FILTER_OPERATION_SEPARATOR}${caseSensitivePrefix}${operation}`;
      } else {
        return `${state.path}${C.FILTER_OPERATION_SEPARATOR}${operation}`;
      }
    case C.ENDS_WITH_TYPE:
      operation = 'endswith';
      if (!state.caseSensitive) {
        return `${state.path}${C.FILTER_OPERATION_SEPARATOR}${caseSensitivePrefix}${operation}`;
      } else {
        return `${state.path}${C.FILTER_OPERATION_SEPARATOR}${operation}`;
      }
  }
};
