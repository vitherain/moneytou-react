import * as C from '../config/constants';

export const getAuthToken = () => {
  return localStorage.getItem(C.LOCAL_STORAGE_AUTH_TOKEN_KEY) || null;
};