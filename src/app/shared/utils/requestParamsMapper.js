import {mapFiltersToRequestParams} from './filterMapper';

export const mapToRequestParams = function (reqConfig, paramsConfig) {
  if (!reqConfig.params) {
    reqConfig.params = {};
  }

  if (paramsConfig.pageNumber) {
    reqConfig.params.page = paramsConfig.pageNumber;
  }
  if (paramsConfig.pageSize) {
    reqConfig.params.size = paramsConfig.pageSize;
  }
  if (paramsConfig.sort && paramsConfig.sort.path && paramsConfig.sort.direction) {
    reqConfig.params.sort = `${paramsConfig.sort.path},${paramsConfig.sort.direction}`;
  }
  reqConfig.params = {...reqConfig.params, ...mapFiltersToRequestParams(paramsConfig.filtersState)};

  return reqConfig;
};
