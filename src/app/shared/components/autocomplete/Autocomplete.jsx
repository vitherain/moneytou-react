import React, {Component} from 'react';

import {Dropdown, DropdownMenu} from 'reactstrap';

import './Autocomplete.scss';

export default class Autocomplete extends Component {

  static defaultProps = {
    options: [],
    matchProperty: null,
    getOptionWrapperClass: () => {
      return '';
    },
    multi: false,
    initialValue: '',
    inputClassName: '',
    onKeyUp: () => {},
    onKeyPress: () => {},
    renderOption: (option) => {
      return option;
    },
    optionPropertyAsValue: null,
    onOptionSelected: () => {},
    renderBefore: () => null,
    renderAfter: () => null
  };

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      options: props.options,
      inputValue: props.initialValue
    };

    this.toggle = this.toggle.bind(this);
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onOptionSelected = this.onOptionSelected.bind(this);
    this.onAdditionalOptionSelected = this.onAdditionalOptionSelected.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  open() {
    this.setState({
      isOpen: true
    });
  }

  close() {
    this.setState({
      isOpen: false
    });
  }

  onInputChange(term) {
    this.setState({ inputValue: term });

    if (term === '') {
      this.setState({ options: this.props.options }, this.close);
      this.onOptionSelected(null);
      return;
    }

    this.setState(
        { options: this.props.options },
        () => {
          const currentOpts = this.state.options;
          const options = currentOpts.filter((opt) => {
            if (this.props.matchProperty) {
              return opt[this.props.matchProperty].toLowerCase().indexOf(term.toLowerCase()) > -1;
            }
            return opt.toLowerCase().indexOf(term.toLowerCase()) > -1;
          });

          if (!options.some(l => l.name.toLowerCase() === term.toLowerCase())) {
            options.push({ isAdditional: true, name: term });
          }
          this.setState({ options: options }, this.open);
        }
    );
  }

  onOptionSelected(option) {
    if (option && this.props.optionPropertyAsValue) {
      this.setState({ inputValue: option[this.props.optionPropertyAsValue] });
    } else if (this.props.multi) {
      this.setState({ inputValue: '' });
    } else if (option) {
      this.setState({ inputValue: option });
    }

    this.close();
    this.props.onOptionSelected(option);
    this.input.focus();
  }

  onAdditionalOptionSelected(option) {
    const newOpt = { ...option };
    delete newOpt['isAdditional'];
    this.setState({ inputValue: '' });
    this.close();
    this.props.onAdditionalOptionSelected(newOpt);
    this.input.focus();
  }

  render() {
    const {
      onKeyUp,
      onKeyPress,
      renderOption,
      renderAdditionalOption,
      getOptionWrapperClass,
      inputClassName,
      placeholder
    } = this.props;
    const { options } = this.state;

    return (
        <Dropdown isOpen={this.state.isOpen} toggle={this.toggle}>
          { this.props.renderBefore ? this.props.renderBefore() : null }

          <input type="search" autoFocus
                 ref={(input) => this.input = input}
                 className={inputClassName}
                 value={this.state.inputValue}
                 placeholder={placeholder}
                 onChange={(event) => this.onInputChange(event.target.value)}
                 onKeyUp={(event) => onKeyUp(event)}
                 onKeyPress={(event) => onKeyPress(event)} />

          { this.props.renderAfter ? this.props.renderAfter() : null }

          <DropdownMenu>
            {
              options.map((opt, index) => {
                if (!opt.isAdditional) {
                  return (
                      <button key={`regularOption${index}`} className={'dropdown-item pointer ' + getOptionWrapperClass(opt, index)}
                              onClick={() => this.onOptionSelected(opt)}>
                        {renderOption(opt, index)}
                      </button>
                  );
                } else {
                  return (
                      <span key={`additionalOptionWrapper${index}`}>
                        { index === 0 ? null : <div className="dropdown-divider" /> }
                        <button key={`additionalOption${index}`} className={'dropdown-item pointer ' + getOptionWrapperClass(opt, index)}
                                onClick={() => this.onAdditionalOptionSelected(opt)}>
                          {renderAdditionalOption(opt, index)}
                        </button>
                      </span>
                  );
                }
              })
            }
          </DropdownMenu>
        </Dropdown>
    );
  }
}
