import React, {Component} from "react";
import moment from 'moment';

class BodyCell extends Component {

    static defaultProps = {
        column: {
            path: '',
            format: {
                type: 'text'
            }
        },
        row: {}
    };

    render() {
        const { row, rowIndex, column, colIndex, column: { format } } = this.props;
        let { column: { classNames, onCellClick } } = this.props;

        if (!classNames) {
            classNames = '';
        }
        if (!onCellClick) {
            onCellClick = () => {};
        }

        if (column.render) {
            return column.render(row, rowIndex, colIndex);
        } else if (column.resolve) {
            let resolved = column.resolve(row, rowIndex, colIndex);
            return (
                <td className={classNames} onClick={(event) => onCellClick(event, row, resolved, rowIndex, colIndex)}>
                    {resolved}
                </td>
            )
        }

        let value = row;

        let pathSplit = column.path.split('.');

        for (let i = 0 ; i < pathSplit.length ; i++) {
            value = value[pathSplit[i]];
        }

        value = this.formatValue(value, format);

        return (
            <td className={classNames} onClick={(event) => onCellClick(event, row, value, rowIndex, colIndex)}>
                {value}
            </td>
        )
    }

    formatValue(value, format) {
        let coefficient;

        switch (format.type) {
            case 'text':
                return value;
            case 'date':
                return moment(value).format(format.format);
            case 'amount':
                if (!format.precision || format.precision === 0) {
                    return `${value} ${format.currency}`;
                }

                coefficient = Math.pow(10, format.precision);
                let amount = parseFloat(Math.round(value * coefficient) / coefficient).toFixed(format.precision);
                return `${amount} ${format.currency}`;
            case 'number':
                if (!format.precision || format.precision === 0) {
                    return value;
                }

                coefficient = Math.pow(10, format.precision);
                return parseFloat(Math.round(value * coefficient) / coefficient).toFixed(format.precision);
        }
    }
}

export default BodyCell;
