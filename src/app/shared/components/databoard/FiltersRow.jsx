import React, {Component} from 'react';
import FilterCell from './FilterCell';

class FiltersRow extends Component {

    static defaultProps = {
        columns: [],
        noFilters: false,
        onFilterStateChange: () => {}
    };

    constructor(props) {
        super(props);
    }

    render() {
        const { noFilters, columns, onFilterStateChange } = this.props;

        if (noFilters) {
            return null;
        }

        return (
            <tr>
                {columns.map((col, index) => {
                    if (!col.filter) {
                        return null;
                    }
                    return (
                        <FilterCell
                            key={`filter${index}`}
                            path={col.path}
                            index={index}
                            filter={col.filter}
                            onFilterStateChange={onFilterStateChange} />
                    )
                })}
            </tr>
        )
    }
}

export default FiltersRow;
