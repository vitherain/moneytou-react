import React, {Component} from "react";

class Title extends Component {

    static defaultProps = {
        title: '',
        noTitle: true
    };

    render() {
        const { title, noTitle } = this.props;

        if (noTitle) {
            return null;
        }

        return (
            <h3 className="display-4">{title}</h3>
        )
    }
}

export default Title;
