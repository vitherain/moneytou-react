import React, { Component } from 'react';

class FAIcon extends Component {

    static defaultProps = {
        onClick: () => {}
    };

    render() {
        const { name, size, className } = this.props;
        let classes = `fa fa-${name}`;

        if (size) {
            classes = `${classes} fa-${size}`;
        }

        if (className) {
            classes = `${classes} ${className}`;
        }

        return <i className={ classes } onClick={this.props.onClick} />
    }
}

export default FAIcon;
