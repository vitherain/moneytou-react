import React, { Component } from 'react';
import { DropdownButton, MenuItem, Button } from 'reactstrap';
import FAIcon from '../FAIcon';

class TextFilter extends Component {

    static CONTAINS_TYPE = 'contains';
    static STARTS_WITH_TYPE = 'starts';
    static ENDS_WITH_TYPE = 'ends';

    static defaultProps = {
        type: 'text',
        onFilterStateChange: () => {},
        defaultMatchType: TextFilter.CONTAINS_TYPE,
        placeholder: ''
    };

    constructor(props) {
        super(props);
        this.state = {
            value: null,
            matchType: props.defaultMatchType,
            caseSensitive: false
        };

        this.callStateChange = this.callStateChange.bind(this);
    }

    render() {
        const { placeholder } = this.props;

        return (
            <div className="input-group input-group-sm">
                <input type="text" className="form-control" placeholder={placeholder}
                    value={this.state.value}
                    onChange={(event) => { this.setState({ value: event.target.value }) }}
                    onBlur={this.callStateChange}
                    onKeyPress={(event) => {
                        if (event.charCode==13) {
                            event.target.blur();
                        }
                    }} />
                <div className="input-group-btn">
                    <Button bsSize="small" onClick={() => { this.setState({ value: null }, this.callStateChange); }}>
                        <FAIcon name="remove" className="pointer-cursor" />
                    </Button>
                    <DropdownButton title={<FAIcon name="filter" />} noCaret bsSize="small">
                        <MenuItem eventKey="1"
                                  onClick={() => { this.setState({ matchType: TextFilter.CONTAINS_TYPE }, this.callStateChange) }}>
                            Contains&nbsp;&nbsp;
                            { this.state.matchType === TextFilter.CONTAINS_TYPE ? <FAIcon name="check" /> : null }
                        </MenuItem>
                        <MenuItem eventKey="2"
                                  onClick={() => { this.setState({ matchType: TextFilter.STARTS_WITH_TYPE }, this.callStateChange) }}>
                            Starts with&nbsp;&nbsp;
                            { this.state.matchType === TextFilter.STARTS_WITH_TYPE ? <FAIcon name="check" /> : null }
                        </MenuItem>
                        <MenuItem eventKey="3"
                                  onClick={() => { this.setState({ matchType: TextFilter.ENDS_WITH_TYPE }, this.callStateChange) }}>
                            Ends with&nbsp;&nbsp;
                            { this.state.matchType === TextFilter.ENDS_WITH_TYPE ? <FAIcon name="check" /> : null }
                        </MenuItem>
                        <MenuItem divider />
                        <MenuItem eventKey="4"
                                  onClick={() => { this.setState({ caseSensitive: !this.state.caseSensitive }, this.callStateChange) }}>
                            <FAIcon name="text-height" /> &nbsp;&nbsp;Case sensitive&nbsp;&nbsp;
                            { this.state.caseSensitive ? <FAIcon name="check" /> : null }
                        </MenuItem>
                    </DropdownButton>
                </div>
            </div>
        )
    }

    callStateChange() {
        let filterState = {
            columnIndex: this.props.index,
            path: this.props.path,
            type: this.props.type,
            value: this.state.value,
            matchType: this.state.matchType,
            caseSensitive: this.state.caseSensitive
        };
        this.props.onFilterStateChange(filterState);
    }
}

export default TextFilter;
