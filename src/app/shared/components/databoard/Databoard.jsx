import React, {Component} from 'react';
import Title from './Title';
import SearchRow from './SearchRow';
import FiltersRow from './FiltersRow';
import HeadRow from './HeadRow';
import Body from './Body';
import Paginator from './Paginator';
import NumberOfRecordsSelector from './NumberOfRecordsSelector';

class Databoard extends Component {

    static defaultProps = {
        config: {
            onTableStateChange: () => {}
        },
        totalElements: 0
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedPage: 1,
            numberOfRecordsOnPage: props.config.numberOfRecordsOnPage || 10,
            sort: props.config.sort || {},
            filtersState: []
        };

        this.onSortingChange = this.onSortingChange.bind(this);
        this.onPageSelect = this.onPageSelect.bind(this);
        this.onNumberOfRecordsOnPageSelect = this.onNumberOfRecordsOnPageSelect.bind(this);
        this.onFilterStateChange = this.onFilterStateChange.bind(this);

        props.config.onTableStateChange(
            this.state.selectedPage,
            this.state.numberOfRecordsOnPage,
            this.state.sort,
            this.state.filtersState
        );
    }

    render() {
        const { config, data, className } = this.props;
        const leftColumnsCount = config.columns.length % 2 === 1
            ? Math.floor(config.columns.length / 2) + 1
            : Math.floor(config.columns.length / 2);
        const rightColumnsCount = config.columns.length - leftColumnsCount;

        return (
            <div>
                <Title noTitle={config.noTitle} title={config.title}/>
                <table className={className}>
                    <thead>
                        <SearchRow
                            columnsLength={config.columns.length}
                            noSearch={config.noSearch}
                            searchTitle={config.searchTitle}
                            noSearchTitle={config.noSearchTitle}
                            searchPlaceholder={config.searchPlaceholder} />
                        <FiltersRow noFilters={config.noFilters} columns={config.columns} onFilterStateChange={this.onFilterStateChange} />
                        <HeadRow columns={config.columns} sort={this.state.sort} onSortingChange={this.onSortingChange} />
                    </thead>
                    <Body numberOfRecordsOnPage={this.state.numberOfRecordsOnPage} columns={config.columns} onRowClick={config.onRowClick} data={data}/>
                    <tfoot>
                        <tr>
                            <td colSpan={leftColumnsCount}>
                                <strong>Total records: </strong><span>{this.props.totalElements}</span>
                            </td>
                            <td colSpan={rightColumnsCount}/>
                        </tr>
                        <tr>
                            <td colSpan={leftColumnsCount} className="align-middle">
                                <Paginator
                                    noPagination={config.noPagination}
                                    maxPages={config.maxNumberOfPages}
                                    selectedPage={this.state.selectedPage}
                                    first={config.showFirstPageSelector}
                                    previous={config.showPreviousPageSelector}
                                    next={config.showNextPageSelector}
                                    last={config.showLastPageSelector}
                                    pageSize={this.state.numberOfRecordsOnPage}
                                    totalElements={this.props.totalElements}
                                    onPageSelect={this.onPageSelect}
                                />
                            </td>
                            <td colSpan={rightColumnsCount} className="align-middle">
                                <NumberOfRecordsSelector
                                    possibleValues={config.numberOfRecordsOnPageValues}
                                    selectedValue={this.state.numberOfRecordsOnPage}
                                    onNumberOfRecordsOnPageSelect={this.onNumberOfRecordsOnPageSelect}
                                />
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        )
    }

    onSortingChange(newSort) {
        this.setState({ sort: newSort }, () => {
            this.props.config.onTableStateChange(
                this.state.selectedPage,
                this.state.numberOfRecordsOnPage,
                this.state.sort,
                this.state.filtersState
            );
        });
    }

    onPageSelect(newSelectedPage) {
        this.setState({ selectedPage: newSelectedPage }, () => {
            this.props.config.onTableStateChange(
                this.state.selectedPage,
                this.state.numberOfRecordsOnPage,
                this.state.sort,
                this.state.filtersState
            );
        });
    }

    onNumberOfRecordsOnPageSelect(newNumberOfRecordsOnPage) {
        if (this.state.numberOfRecordsOnPage < newNumberOfRecordsOnPage) {
            this.setState({ selectedPage: 1 }, () => {
                this.props.config.onTableStateChange(this.state.selectedPage, newNumberOfRecordsOnPage, this.state.sort, this.state.filtersState);
            });
        } else {
            this.props.config.onTableStateChange(this.state.selectedPage, newNumberOfRecordsOnPage, this.state.sort, this.state.filtersState);
        }

        this.setState({numberOfRecordsOnPage: newNumberOfRecordsOnPage});
    }

    onFilterStateChange(newFilterState) {
        let currentFiltersStateCopy = this.state.filtersState.slice();
        let filterIndex = currentFiltersStateCopy.findIndex((element) => element.path === newFilterState.path);

        if (filterIndex !== -1) {
            if (newFilterState.value) {
                currentFiltersStateCopy[filterIndex] = newFilterState;
            } else {
                currentFiltersStateCopy.splice(filterIndex, 1);
            }
        } else if (newFilterState.value) {
            currentFiltersStateCopy.push(newFilterState);
        }

        this.setState({ filtersState: currentFiltersStateCopy, selectedPage: 1 }, () => {
            this.props.config.onTableStateChange(
                this.state.selectedPage,
                this.state.numberOfRecordsOnPage,
                this.state.sort,
                this.state.filtersState
            );
        });
    }
}

export default Databoard;
