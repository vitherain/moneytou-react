import React, {Component} from "react";
import BodyRow from "./BodyRow";

class Body extends Component {

    static defaultProps = {
        title: '',
        columns: [],
        data: []
    };

    render() {
        const { numberOfRecordsOnPage, columns, data, onRowClick } = this.props;

        return (
            <tbody>
                {data.map((row, rowIndex) => {
                    if (rowIndex < numberOfRecordsOnPage) {
                        return <BodyRow key={`row${rowIndex}`} index={rowIndex} columns={columns} row={row} onRowClick={onRowClick} />
                    }
                    return null;
                })}
            </tbody>
        )
    }
}

export default Body;
