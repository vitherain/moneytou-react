import React, {Component} from 'react';
import HeadCell from './HeadCell';

class HeadRow extends Component {

    static defaultProps = {
        columns: []
    };

    render() {
        const { columns, sort, onSortingChange } = this.props;

        return (
            <tr>
                {columns.map((col, index) => {
                    return (
                        <HeadCell
                            key={`headCell${index}`}
                            sortBy={sort.columnIndex === index}
                            sortDirection={sort.columnIndex === index ? sort.direction : 'none'}
                            index={index}
                            title={col.title}
                            noTitle={col.noTitle}
                            path={col.sortPath || col.path}
                            noSort={col.noSort}
                            format={col.format}
                            onSortingChange={onSortingChange} />
                    )
                })}
            </tr>
        )
    }
}

export default HeadRow;
