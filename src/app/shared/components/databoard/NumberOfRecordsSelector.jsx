import React, {Component} from 'react';

class NumberOfRecordsSelector extends Component {

    static defaultProps = {
        possibleValues: [5, 10, 15, 25, 50, 100],
        onNumberOfRecordsOnPageSelect: () => {}
    };

    constructor() {
        super();
        this.onChange = this.onChange.bind(this);
    }

    render() {
        return (
            <div className="form-group row">
                <label className="col-3 control-label hidden-sm hidden-xs">
                    Number of records
                </label>
                <div className="col-9 col-md-6">
                    <select className="custom-select form-control form-control-sm" onChange={this.onChange} value={this.props.selectedValue}>
                        {this.props.possibleValues.map((value) => {
                            return (<option key={value} value={value}>{value}</option>)
                        })}
                    </select>
                </div>
            </div>
        );
    }

    onChange(event) {
        let numberOfRecords = parseInt(event.target.value, 10);
        this.props.onNumberOfRecordsOnPageSelect(numberOfRecords, this.props);
    }
}

export default NumberOfRecordsSelector;
