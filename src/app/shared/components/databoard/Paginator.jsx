import React, {Component} from 'react';

class Paginator extends Component {

    static defaultProps = {
        noPagination: false,
        maxPages: 5,
        first: true,
        last: true,
        previous: true,
        next: true,
        onPageSelect: () => {}
    };

    handleGeneralPageSelect(type) {
        let totalPages = this.resolveNumberOfFictivePages();

        switch(type) {
            case 'first':
                this.handleRegularPageSelect(1);
                break;
            case 'last':
                this.handleRegularPageSelect(totalPages);
                break;
            case 'previous':
                let previousIndex = this.props.selectedPage - 1;
                this.handleRegularPageSelect(previousIndex);
                break;
            case 'next':
                let nextIndex = this.props.selectedPage + 1;
                this.handleRegularPageSelect(nextIndex);
                break;
        }
    }

    handleRegularPageSelect(pageNumber) {
        if (this.props.selectedPage === pageNumber) {
            return;
        }
        if (pageNumber === 0) {
            return;
        }
        if (pageNumber > this.resolveNumberOfFictivePages()) {
            return;
        }

        this.props.onPageSelect(pageNumber, this.props);
    }

    render() {
        if (this.props.noPagination) {
            return null;
        }

        return (
            <nav>
                <ul className="pagination pagination-sm">
                    { this.resolveGeneralButton('first') }
                    { this.resolveGeneralButton('previous') }
                    { this.resolveRegularButtons() }
                    { this.resolveGeneralButton('next') }
                    { this.resolveGeneralButton('last') }
                </ul>
            </nav>
        );
    }

    resolveGeneralButton(type) {
        let totalPages = this.resolveNumberOfFictivePages();
        let opts = {};
        if (this.props.selectedPage === totalPages && (type === 'next' || type === 'last')) {
            opts.disabled = true;
        }
        if (this.props.selectedPage === 1 && (type === 'first' || type === 'previous')) {
            opts.disabled = true;
        }

        if (this.props[type]) {
            return (
                <li className={`page-item ${opts.disabled ? 'disabled' : ''}`}>
                    <a className="page-link" href="#" onClick={() => this.handleGeneralPageSelect(type)}>
                        { type.substring(0, 1).toUpperCase() + type.substring(1, type.length)}
                    </a>
                </li>
            )
        }
    }

    resolveRegularButtons() {
        let buttons = [];

        for (let i = 1 ; i <= this.resolveMaxPages() ; i++) {
            let btn =
                <li key={`pg${i}`} className={`page-item ${this.props.selectedPage === i ? 'active' : ''}`} >
                    { this.props.selectedPage === i ?
                        <span className="page-link">{ i }</span> :
                        <a className="page-link" href="#" onClick={() => this.handleRegularPageSelect(i)}>{ i }</a>
                    }
                </li>;
            buttons.push(btn);
        }

        return buttons;
    }

    resolveMaxPages() {
        if (this.props.totalElements <= this.props.pageSize) {
            return 1;
        }

        let itemsToShownItems = Math.floor(this.props.totalElements / this.props.pageSize);
        let remainder = this.props.totalElements % this.props.pageSize;

        if (remainder > 0) {
            itemsToShownItems++;
        }

        return Math.min(itemsToShownItems, this.props.maxPages);
    }

    resolveNumberOfFictivePages() {
        if (this.props.totalElements <= this.props.pageSize) {
            return 1;
        }

        let itemsToShownItems = Math.floor(this.props.totalElements / this.props.pageSize);
        let remainder = this.props.totalElements % this.props.pageSize;

        if (remainder > 0) {
            itemsToShownItems++;
        }

        return itemsToShownItems;
    }
}

export default Paginator;
