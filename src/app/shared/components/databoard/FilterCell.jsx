import React, {Component} from 'react';
import TextFilter from './filters/TextFilter';

class FilterCell extends Component {

    static defaultProps = {
        path: null,
        filter: {},
        onFilterStateChange: () => {}
    };

    render() {
        const { path, index, filter, onFilterStateChange } = this.props;

        switch (filter.type) {
            case 'text':
                return (
                    <th>
                        <TextFilter
                            path={filter.path || path}
                            index={index}
                            placeholder={filter.placeholder}
                            defaultMatchType={filter.defaultMatchType}
                            onFilterStateChange={onFilterStateChange} />
                    </th>
                )
        }

        return <td/>
    }
}

export default FilterCell;
