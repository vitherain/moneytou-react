import React, {Component} from 'react';
import FAIcon from './FAIcon';

class HeadCell extends Component {

    static defaultProps = {
        index: 0,
        title: '',
        noTitle: false,
        path: '',
        noSort: false,
        format: null,
        onSortingChange: () => {}
    };

    constructor() {
        super();
        this.onClick = this.onClick.bind(this);
        this.resolveNewSortDirection = this.resolveNewSortDirection.bind(this);
    }

    render() {
        const { index, title, noTitle, path, noSort, sortBy, sortDirection, format } = this.props;

        let classNames = noSort ? '' : 'pointer-cursor';

        if (noTitle) {
            return <th onClick={this.onClick} className={classNames}>{this.resolveSortingIcon()}</th>
        }

        return <th onClick={this.onClick} className={classNames}>{title}&nbsp;&nbsp;{this.resolveSortingIcon()}</th>
    }

    onClick() {
        if (this.props.noSort) {
            return;
        }

        let direction = this.resolveNewSortDirection();

        if (direction === 'none') {
            this.props.onSortingChange({}, this.props);
        } else {
            let sort = {
                columnIndex: this.props.index,
                path: this.props.path,
                direction: direction
            };
            this.props.onSortingChange(sort, this.props);
        }
    }

    resolveNewSortDirection() {
        switch (this.props.sortDirection) {
            case 'none':
                return 'asc';
            case 'asc':
                return 'desc';
            case 'desc':
                return 'none';
        }
    }

    resolveSortingIcon() {
        const { sortBy, sortDirection, noSort } = this.props;

        if (noSort) {
            return null;
        }

        let name = '';
        switch (sortDirection.toLowerCase()) {
            case 'asc':
                name = 'sort-asc';
                break;
            case 'desc':
                name = 'sort-desc';
                break;
        }

        if (!sortBy) {
            return <FAIcon name="sort" />
        }
        return <FAIcon name={name} />
    }
}

export default HeadCell;
