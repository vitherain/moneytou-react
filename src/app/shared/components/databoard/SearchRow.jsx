import React, {Component} from 'react';

class SearchRow extends Component {

    static defaultProps = {
        columnsLength: 0,
        noSearch: false,
        searchTitle: null,
        noSearchTitle: false,
        searchPlaceholder: null
    };

    render() {
        const { columnsLength } = this.props;
        const leftColumnsCount = columnsLength % 2 === 1
            ? Math.floor(columnsLength / 2) + 1
            : Math.floor(columnsLength / 2);
        const rightColumnsCount = columnsLength - leftColumnsCount;

        if (this.props.noSearch) {
            return null;
        }

        return (
            <tr>
                <td colSpan={leftColumnsCount} />
                <td colSpan={rightColumnsCount}>
                    <div className="form-group row">
                        {this.resolveSearchTitle()}
                        <div className="col-9 col-md-6">
                            <input className="form-control form-control-sm" type="text" placeholder={this.props.searchPlaceholder} />
                        </div>
                    </div>
                </td>
            </tr>
        )
    }

    resolveSearchTitle() {
        if (this.props.noSearchTitle) {
            return null;
        }
        return (
            <label className="col-3 col-form-label col-form-label-sm">
                <strong>
                    {this.props.searchTitle}
                </strong>
            </label>
        )
    }
}

export default SearchRow;
