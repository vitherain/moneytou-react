import React, {Component} from "react";
import BodyCell from "./BodyCell";

class BodyRow extends Component {

    static defaultProps = {
        columns: [],
        row: {},
        onRowClick: () => {}
    };

    render() {
        const { row, index, columns, onRowClick } = this.props;

        return (
            <tr onClick={(event) => onRowClick(event, row, index)}>
                {columns.map((col, colIndex) => {
                    return <BodyCell key={`col${colIndex}`} column={col} row={row} rowIndex={index} colIndex={colIndex} />
                })}
            </tr>
        )
    }
}

export default BodyRow;
