import React, {Component} from "react";

class FAIcon extends Component {
  render() {
    const {name, size} = this.props;
    let classes = `fa fa-${name}`;

    if (size) {
      classes = `${classes} fa-${size}`;
    }

    return <i className={ classes }/>
  }
}

export default FAIcon;
