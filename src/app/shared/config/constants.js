export const YEAR_OF_CREATION = 2016;

export const DEFAULT_CONTENT_TYPE = 'application/json';

export const X_AUTH_HEADER_NAME = 'X-Auth-Token';
export const AUTHORIZATION_HEADER_NAME = 'Authorization';
export const LOCAL_STORAGE_AUTH_TOKEN_KEY = 'X-Auth-Token';
export const LOCAL_STORAGE_CURRENT_USER_KEY = 'current-user';

export const GET_METHOD = 'get';
export const POST_METHOD = 'post';
export const PUT_METHOD = 'put';
export const DELETE_METHOD = 'delete';

export const CONTAINS_TYPE = 'contains';
export const STARTS_WITH_TYPE = 'starts';
export const ENDS_WITH_TYPE = 'ends';
export const FILTER_OPERATION_SEPARATOR = '__';