import React, {Component} from "react";
import FAIcon from "../../../shared/components/FAIcon";
import * as C from "../../../shared/config/constants";
import {getCurrentYear} from "../../../shared/utils/timeMachine";

class Footer extends Component {

  render() {
    return (
        <section id="footer">
          <div className="container">
            <div className="row">
              <div className="col-md-8 col-sm-6">
                <h3>About Moneytou</h3>
                <p>Moneytou is Personal Financial Management app.</p>
                <p>You can use it to register your incomes and expenses, manage loans and analyze all the data.</p>
              </div>

              <div className="col-md-4 col-sm-6">
                <h3>Contact me</h3>
                <ul className="list-unstyled">
                  <li><p><strong><FAIcon name="envelope"/> Email:</strong> <a href="mailto:vit.herain@gmail.com">vit.herain@gmail.com</a>
                  </p></li>
                </ul>
              </div>
            </div>

            <div className="row">
              <div className="col-md-12">
                <Copyright/>
              </div>
            </div>
          </div>
        </section>
    );
  }
}

function Copyright() {
  const startYear = C.YEAR_OF_CREATION;
  const currentYear = getCurrentYear();
  let copyright = null;
  if (startYear === currentYear) {
    copyright = <p>&copy; 2016 Created by Vít Herain</p>;
  } else {
    copyright = <p>&copy; 2016 - {currentYear} Created by Vít Herain</p>;
  }

  return copyright;
}

export default Footer;
