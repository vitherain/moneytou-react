import React, {Component} from "react";
import {Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {IndexLink, Link} from "react-router";
import FAIcon from "../../../shared/components/FAIcon";

import {connect} from 'react-redux';
import {apiPostLogout} from '../../../auth/actions/actions';

class Navigation extends Component {

  constructor(props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.resolveRightPart = this.resolveRightPart.bind(this);
    this.resolveLeftPart = this.resolveLeftPart.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  resolveLeftPart() {
    const { isAuthenticated } = this.props;

    if (isAuthenticated) {
      return (
        <Nav navbar>
          <NavItem>
            <NavLink tag={Link} to="/cash-flow" activeClassName="active">
                <FAIcon name="line-chart" /> Cash flow
            </NavLink>
          </NavItem>
        </Nav>
      );
    } else {
      return (
        <Nav navbar>
          <NavItem>
            <NavLink tag={IndexLink} to="/" activeClassName="active">Home</NavLink>
          </NavItem>
        </Nav>
      );
    }
  }

  resolveRightPart() {
    const { isAuthenticated, apiPostLogout } = this.props;

    if (isAuthenticated) {
      return (
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink className="pointer-cursor" onClick={() => apiPostLogout()}>
                  <FAIcon name="sign-out" /> Logout
              </NavLink>
            </NavItem>
          </Nav>
      );
    } else {
      return (
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink tag={Link} to="/login" activeClassName="active">
                  <FAIcon name="sign-in" /> Login
              </NavLink>
            </NavItem>
          </Nav>
      );
    }
  }

  render() {
    return (
        <Navbar color="faded" light toggleable className="navbar-expand-md">
          <NavbarToggler right onClick={this.toggleNavbar} />
          <Collapse isOpen={!this.state.collapsed} navbar>
            <NavbarBrand tag={Link} to="/"><FAIcon name="usd" /> Moneytou</NavbarBrand>
            { this.resolveLeftPart() }
            { this.resolveRightPart() }
          </Collapse>
        </Navbar>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isAuthenticated: state.isAuthenticated
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    apiPostLogout: () => dispatch(apiPostLogout())
  }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navigation);
