import React, {Component} from "react";

import Navigation from '../navigation/Navigation';
import Footer from '../footer/Footer';

class App extends Component {

  render() {
    return (
        <div>
          <Navigation/>

          <section id="main">
            <div className="container">
              { this.props.children }
            </div>
          </section>

          <Footer/>
        </div>
    );
  }
}

export default App;
