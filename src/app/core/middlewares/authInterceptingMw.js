import {REJECTED} from 'redux-promise-middleware'
import {browserHistory} from "react-router";
import {apiPostLogout} from '../../auth/actions/actions';

const authInterceptingMw = store => next => action => {
  next(action);

  if (action.type.startsWith('API_') && action.type.endsWith(`_${REJECTED}`)) {
    if (action.payload && (action.payload.status === 401 || action.payload.status === 403)) {
      store.dispatch(apiPostLogout()).then(() => browserHistory.push('/login'));
    }
  }
};

export default authInterceptingMw;