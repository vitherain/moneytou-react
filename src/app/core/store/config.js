import {applyMiddleware, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import authInterceptingMw from '../middlewares/authInterceptingMw';
import combineReducers from '../reducers/index';

import {autoRehydrate} from 'redux-persist';

const store = createStore(
    combineReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    compose(
        applyMiddleware(
            thunk,
            promiseMiddleware(),
            authInterceptingMw
        ),
        autoRehydrate()
    )
);
export default store;
