import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import {reducer as formReducer} from 'redux-form';

import {expenses} from '../../cash-flow/reducers/expenses';
import {selectedCostMonthId} from '../../cash-flow/reducers/selectedCostMonthId';
import {user} from './user';
import {isAuthenticated} from './isAuthenticated';
import {costYears} from '../../cash-flow/reducers/costYears';
import {recipients} from '../../cash-flow/reducers/recipients';
import {editedExpensesTableCells} from '../../cash-flow/reducers/editedExpensesTableCells';
import {labels} from "../../cash-flow/reducers/labels";

export default combineReducers({
  routing: routerReducer,
  form: formReducer,
  user,
  isAuthenticated,
  costYears,
  recipients,
  labels,
  expenses,
  selectedCostMonthId,
  editedExpensesTableCells
});
