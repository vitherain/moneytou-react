import * as A from '../../auth/actions/actions';
import {FULFILLED} from 'redux-promise-middleware';

export const user = (state = {}, action) => {
  switch (action.type) {
    case `${A.API_POST_LOGIN}_${FULFILLED}`:
      return action.payload;
    case `${A.API_POST_LOGOUT}_${FULFILLED}`:
      return {};
    default:
      return state;
  }
};
