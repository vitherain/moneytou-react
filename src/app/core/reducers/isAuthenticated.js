import * as A from '../../auth/actions/actions';
import {FULFILLED} from 'redux-promise-middleware';

export const isAuthenticated = (state = false, action) => {
  switch (action.type) {
    case `${A.API_POST_LOGIN}_${FULFILLED}`:
      return true;
    case `${A.API_POST_LOGOUT}_${FULFILLED}`:
      return false;
    default:
      return state;
  }
};
