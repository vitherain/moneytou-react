// Bootstrap 4
require('../../node_modules/bootstrap/dist/css/bootstrap.min.css');

// Moneytou direct styles
require('../static/css/styles.scss');
// Google fonts
require('../static/css/google.font.alegreya.css');
// Fontface / Font Awesome
require('font-awesome/scss/font-awesome.scss');
// Normalize.css
require('normalize.css/normalize.css');

import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {Router, Route, IndexRoute, browserHistory} from 'react-router';
import {syncHistoryWithStore} from 'react-router-redux';
import store from './core/store/config';

import {persistStore} from 'redux-persist';

import routes from './routes';

import * as C from "./shared/config/constants";
import Axios from "axios";

const history = syncHistoryWithStore(browserHistory, store);

// begin periodically persisting the store
persistStore(store, {whitelist: ['isAuthenticated', 'user']});

Axios.defaults.headers.post['Content-Type'] = C.DEFAULT_CONTENT_TYPE;

render(
    <Provider store={store}>
      <Router history={history} routes={routes} onUpdate={() => window.scrollTo(0, 0)}/>
    </Provider>,
    document.getElementById('root')
);
