import * as A from '../actions/actions';
import {FULFILLED} from 'redux-promise-middleware';

export const expenses = (state = {content: [], totalElements: 0}, action) => {
  switch (action.type) {
    case `${A.API_GET_EXPENSES}_${FULFILLED}`:
      return action.payload;
    default:
      return state;
  }
};
