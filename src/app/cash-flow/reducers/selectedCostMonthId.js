import * as A from '../actions/actions';
import {FULFILLED} from 'redux-promise-middleware';

export const selectedCostMonthId = (state = null, action) => {
  switch (action.type) {
    case `${A.SELECT_CASH_FLOW_COST_MONTH}_${FULFILLED}`:
      return action.selectedCostMonthId;
    default:
      return state;
  }
};
