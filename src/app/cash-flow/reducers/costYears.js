import * as A from '../actions/actions';
import {FULFILLED} from 'redux-promise-middleware';

export const costYears = (state = [], action) => {
  switch (action.type) {
    case `${A.API_GET_COST_YEARS_SORTED_BY_YEAR_DESC}_${FULFILLED}`:
      return action.payload;
    default:
      return state;
  }
};
