import * as A from '../actions/actions';
import {FULFILLED} from 'redux-promise-middleware';

export const editedExpensesTableCells = (state = [], action) => {
  switch (action.type) {
    case `${A.EDIT_EXPENSES_TABLE_CELL}_${FULFILLED}`:
      let stateCopy = state.slice();
      stateCopy.push({
        rowIndex: action.rowIndex,
        colIndex: action.colIndex
      });
      return stateCopy;
    case `${A.RESET_EXPENSES_TABLE_CELL}_${FULFILLED}`:
      return [];
    default:
      return state;
  }
};
