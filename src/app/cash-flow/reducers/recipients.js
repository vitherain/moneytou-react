import * as A from '../actions/actions';
import {FULFILLED} from 'redux-promise-middleware';

export const recipients = (state = [], action) => {
  switch (action.type) {
    case `${A.API_GET_RECIPIENTS}_${FULFILLED}`:
      return action.payload;
    default:
      return state;
  }
};
