import React, {Component} from "react";
import {connect} from "react-redux";
import Autocomplete from '../../../shared/components/autocomplete/Autocomplete';
import FAIcon from '../../../shared/components/FAIcon';
import ExpenseWizardProgress from '../../utils/ExpenseWizardProgress';
import ItemType from '../../utils/ItemType';
import {
  Input,
  Button,
  InputGroup,
  InputGroupButton,
  InputGroupAddon
} from 'reactstrap';
import ExpenseUtil from '../../utils/ExpenseUtil';

import './NewCashFlowItemView.scss';

import {getFaIconName, getLabelStyle} from '../../utils/LabelUtil';
import {
  apiGetUsersMeLabels,
  apiPostUsersMeLabel,
  apiGetUsersMeRecipients,
  apiPostUsersMeRecipient,
  apiPostUsersMeExpense
} from "../../actions/actions";

import BEMHelper from 'react-bem-helper';

const classes = new BEMHelper({
  name: 'new-cash-flow-item'
});

class NewCashFlowItemView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      type: ItemType.EXPENSE,
      expenseStep: ExpenseWizardProgress.AMOUNT,
      expense: {
        date: new Date(),
        labels: []
      }
    };

    this.goToPreviousStep = this.goToPreviousStep.bind(this);
    this.goToNextStep = this.goToNextStep.bind(this);
    this.onKeyUp = this.onKeyUp.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
    this.onAmountChange = this.onAmountChange.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
    this.onLabelAdded = this.onLabelAdded.bind(this);
    this.onLabelCreated = this.onLabelCreated.bind(this);
    this.onRecipientChange = this.onRecipientChange.bind(this);
    this.onRecipientCreated = this.onRecipientCreated.bind(this);
    this.onNoteChange = this.onNoteChange.bind(this);
    this.onRemoveLabel = this.onRemoveLabel.bind(this);
    this.onRecipientRemove = this.onRecipientRemove.bind(this);
    this.onFinishExpense = this.onFinishExpense.bind(this);
    this.renderRemoveRecipientButton = this.renderRemoveRecipientButton.bind(this);
  }

  componentDidMount() {
    this.props.apiGetUsersMeLabels();
    this.props.apiGetUsersMeRecipients();
  }

  reload() {
    this.setState({
      expenseStep: ExpenseWizardProgress.AMOUNT,
      expense: {
        date: new Date(),
        labels: []
      }
    });
  }

  onItemTypeChange() {
    console.log('onItemTypeChange triggered');
  }

  goToPreviousStep() {
    if (this.state.type === ItemType.EXPENSE) {
      const prevStep = ExpenseUtil.getPreviousStep(this.state.expenseStep);

      if (prevStep) {
        this.setState({ expenseStep: prevStep });
      }
    }
  }

  goToNextStep() {
    if (this.state.type === ItemType.EXPENSE) {
      if (this.state.expenseStep === ExpenseWizardProgress.NOTE) {
        this.onFinishExpense();
      }

      const nextStep = ExpenseUtil.getNextStep(this.state.expenseStep);
      if (nextStep) {
        this.setState({ expenseStep: nextStep });
      }
    }
  }

  onRecipientRemove() {
    if (this.state.type === ItemType.EXPENSE) {
      const expense = { ...this.state.expense, ...{ recipient: null }};
      this.setState({ expense: expense });
    }
  }

  onKeyUp(event) {
    if (event.ctrlKey && event.key.toLowerCase() === 'backspace') {
      this.goToPreviousStep();
    }
  }

  onKeyPress(event) {
    if (event.key.toLowerCase() === 'enter') {
      this.goToNextStep();
    }
  }

  onAmountChange(amount) {
    const expense = { ...this.state.expense, ...{ amount: { value: amount } }};
    this.setState({ expense: expense });
  }

  onDateChange(date) {
    const expense = { ...this.state.expense, ...{ date }};
    this.setState({ expense: expense });
  }

  onLabelAdded(label) {
    if (!this.state.expense.labels.some(l => l.id === label.id)) {
      const labels = this.state.expense.labels || [];
      labels.push(label);
      const expense = { ...this.state.expense, ...{ labels }};
      this.setState({ expense: expense });
    }
  }

  onLabelCreated(newLabel) {
    this.props.apiPostUsersMeLabel(newLabel)
        .then(({ value }) => newLabel.id = value.id)
        .then(this.props.apiGetUsersMeLabels)
        .then(() => {
          const newSavedLabel = this.props.labels.find((l) => l.id === newLabel.id);
          this.onLabelAdded(newSavedLabel);
        });
  }

  onRecipientChange(recipient) {
    const expense = { ...this.state.expense, ...{ recipient }};
    this.setState({ expense: expense });
  }

  onRecipientCreated(newRecipient) {
    this.props.apiPostUsersMeRecipient(newRecipient)
        .then(({ value }) => newRecipient.id = value.id)
        .then(this.props.apiGetUsersMeRecipients)
        .then(() => {
          const newSavedRecipient = this.props.recipients.find((r) => r.id === newRecipient.id);
          this.onRecipientChange(newSavedRecipient);
        });
  }

  onNoteChange(note) {
    const expense = { ...this.state.expense, ...{ note }};
    this.setState({ expense: expense });
  }

  onRemoveLabel(label) {
    if (this.state.type === ItemType.EXPENSE) {
      const labels = this.state.expense.labels.filter((item) => item.id !== label.id);
      const expense = { ...this.state.expense, ...{ labels }};
      this.setState({ expense: expense });
    }
  }

  onFinishExpense() {
    const expense = this.state.expense;
    this.props.apiPostUsersMeExpense(expense).then(this.reload());
  }

  renderLabelOption(label) {
    return (
        <span>
          <FAIcon name={getFaIconName(label)} />
          &nbsp;{ label.name }
          &nbsp;
          <span className="label-color-sample" style={{backgroundColor: label.backgroundColor, color: label.textColor}}>a</span>
        </span>
    );
  }

  renderAdditionalLabelOption(label) {
    return (
        <span>
          <FAIcon name="plus" />
          &nbsp;<em>{ `Create ${ label.name } label` }</em>
        </span>
    );
  }

  renderRecipientOption(recipient) {
    return recipient.name;
  }

  renderAdditionalRecipientOption(recipient) {
    return (
        <span>
          <FAIcon name="plus" />
          &nbsp;<em>{ `Create ${ recipient.name } recipient` }</em>
        </span>
    );
  }

  renderRemoveRecipientButton() {
    if (this.state.expense.recipient) {
      return (
          <InputGroupButton>
            <Button color="light"
                    className="pointer-cursor"
                    onClick={() => this.onRecipientRemove()}>
              <FAIcon name="times"/>
            </Button>
          </InputGroupButton>
      );
    } else {
      return null;
    }
  }

  render() {
    const { expense, expenseStep } = this.state;
    const { labels, recipients } = this.props;

    return (
        <InputGroup size="lg" {...classes()}>
          <InputGroupButton>
            <Button color="light"
                    className="pointer-cursor"
                    onClick={() => this.onItemTypeChange()}>
              <FAIcon name="minus"/>
            </Button>
          </InputGroupButton>
          <InputGroupButton>
            <Button color="light"
                    className="pointer-cursor"
                    disabled={expenseStep === ExpenseWizardProgress.AMOUNT}
                    onClick={() => this.goToPreviousStep()}>
              <FAIcon name="arrow-left"/>
            </Button>
          </InputGroupButton>

          {
            expenseStep === ExpenseWizardProgress.AMOUNT
            ?  <Input type="number" autoFocus placeholder="What amount did you pay?"
                      value={expense.amount ? expense.amount.value : ''}
                      onChange={(event) => this.onAmountChange(event.target.value)}
                      onKeyUp={(event) => this.onKeyUp(event)} onKeyPress={(event) => this.onKeyPress(event)} />
            : null
          }
          {
            expenseStep === ExpenseWizardProgress.AMOUNT
            ? <InputGroupAddon {...classes('addon')}>Kč</InputGroupAddon>
            : null
          }

          {
            expenseStep === ExpenseWizardProgress.DATE
                ? <Input type="date" autoFocus
                         placeholder="When was that?"
                         value={expense.date || ''}
                         onChange={(event) => this.onDateChange(event.target.value)}
                         onKeyUp={(event) => this.onKeyUp(event)}
                         onKeyPress={(event) => this.onKeyPress(event)} />
                : null
          }

          {
            expenseStep === ExpenseWizardProgress.LABELS
            ? ( expense.labels.map(
                (label, index) => {
                  return (
                      <span key={index} className="badge" style={getLabelStyle(label)}>
                        <FAIcon name={getFaIconName(label)}/>&nbsp;{label.name}
                        &nbsp;<span className="pointer" onClick={() => this.onRemoveLabel(label)}>&times;</span>
                      </span>
                  );
                })
              )
            : null
          }
          {
            expenseStep === ExpenseWizardProgress.LABELS
                ? <Autocomplete options={labels}
                                multi={true}
                                matchProperty="name"
                                inputClassName="form-control"
                                renderOption={this.renderLabelOption}
                                renderAdditionalOption={this.renderAdditionalLabelOption}
                                placeholder="What did you pay for?"
                                onOptionSelected={(label) => this.onLabelAdded(label)}
                                onAdditionalOptionSelected={(newLabel) => this.onLabelCreated(newLabel)}
                                onKeyUp={(event) => this.onKeyUp(event)}
                                onKeyPress={(event) => this.onKeyPress(event)} />
                : null
          }

          {
            expenseStep === ExpenseWizardProgress.RECIPIENT
            ? <Autocomplete options={recipients}
                            initialValue={expense.recipient ? expense.recipient.name : ''}
                            multi={false}
                            matchProperty="name"
                            inputClassName="form-control"
                            renderOption={this.renderRecipientOption}
                            renderAdditionalOption={this.renderAdditionalRecipientOption}
                            optionPropertyAsValue="name"
                            placeholder="Who did you pay?"
                            onOptionSelected={(recipient) => this.onRecipientChange(recipient)}
                            onAdditionalOptionSelected={(newRecipient) => this.onRecipientCreated(newRecipient)}
                            onKeyUp={(event) => this.onKeyUp(event)}
                            onKeyPress={(event) => this.onKeyPress(event)}
                            renderAfter={this.renderRemoveRecipientButton} />
            : null
          }

          {
            expenseStep === ExpenseWizardProgress.NOTE
            ? <Input type="text" autoFocus
                     placeholder="Any note?"
                     value={expense.note || ''}
                     onChange={(event) => this.onNoteChange(event.target.value)}
                     onKeyUp={(event) => this.onKeyUp(event)}
                     onKeyPress={(event) => this.onKeyPress(event)} />
            : null
          }

          <InputGroupButton>
            <Button color={expenseStep === ExpenseWizardProgress.NOTE ? 'primary' : 'light'}
                    className="pointer-cursor"
                    onClick={() => expenseStep === ExpenseWizardProgress.NOTE ? this.onFinishExpense() : this.goToNextStep()}>
              <FAIcon name="arrow-right"/>
            </Button>
          </InputGroupButton>
        </InputGroup>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    labels: state.labels,
    recipients: state.recipients
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    apiGetUsersMeLabels: () => dispatch(apiGetUsersMeLabels()),
    apiPostUsersMeLabel: (newLabel) => dispatch(apiPostUsersMeLabel(newLabel)),
    apiGetUsersMeRecipients: () => dispatch(apiGetUsersMeRecipients()),
    apiPostUsersMeRecipient: (newRecipient) => dispatch(apiPostUsersMeRecipient(newRecipient)),
    apiPostUsersMeExpense: (expense) => dispatch(apiPostUsersMeExpense(expense))
  }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewCashFlowItemView);
