import React, {Component} from 'react';
import {Tab, Row, Col, Nav, NavItem, NavDropdown, MenuItem, Button} from 'reactstrap';
import ExpensesTable from '../expenses-table/ExpensesTable';
import FAIcon from '../../../shared/components/FAIcon';

import {connect} from 'react-redux';
import {
  apiGetUsersMeCostYearsSortedByYearDesc,
  apiPostCostMonth,
  selectCashFlowCostMonth
} from '../../actions/actions';

class CashFlowView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showAddInput: false,
      addInputValue: null
    };
  }

  componentDidMount() {
    this.props.apiGetUsersMeCostYearsSortedByYearDesc();
  }

  render() {
    const {costYears, selectCashFlowCostMonth, costMonthKey, apiPostCostMonth} = this.props;

    if (costYears.length) {
      let defaultActiveKey = this.getDefaultActiveKey(costYears);
      if (!costMonthKey) {
        selectCashFlowCostMonth(defaultActiveKey);
      }

      return (
          <div>
            <Tab.Container defaultActiveKey={ defaultActiveKey }>
              <Row>
                <Tab.Content animation mountOnEnter={true}>
                  {costYears.map((costYear) => {
                    return costYear.costMonths.map((costMonth) => {
                      return (
                          <Tab.Pane eventKey={`${costYear.id}.${costMonth.id}`}>
                            <ExpensesTable/>
                          </Tab.Pane>
                      )
                    });
                  })}
                </Tab.Content>

                <Nav bsStyle="tabs" onSelect={(key) => {
                  selectCashFlowCostMonth(key)
                }}>
                  <li role="presentation">
                    <a role="tab" href="javascript:void(0)" eventKey="plus"
                       onClick={(event) => {
                         if (!this.state.showAddInput)
                           this.setState({showAddInput: !this.state.showAddInput});

                         event.stopPropagation();
                       }}>
                      {
                        !this.state.showAddInput
                            ? <FAIcon name="plus"/>
                            : <input type="text" value={this.state.addInputValue}
                                     onChange={(event) => this.setState({addInputValue: event.target.value})}
                                     onBlur={() => {
                                       console.log('blur occurred')
                                       apiPostCostMonth(this.state.addInputValue.split('/')[0], this.state.addInputValue.split('/')[1]);
                                     }}
                                     onKeyPress={(event) => {
                                       if (event.charCode == 13) {
                                         event.target.blur();
                                       }
                                     }}/>
                      }
                    </a>
                  </li>

                  {costYears.map((costYear, yearIndex) => {
                    if (yearIndex === 0) {
                      return costYear.costMonths.map((costMonth) => {
                        return (
                            <NavItem key={`${costYear.id}.${costMonth.id}`} eventKey={`${costYear.id}.${costMonth.id}`}>
                              { `${costMonth.monthInYear}/${costMonth.year}` }
                            </NavItem>
                        )
                      })
                    } else {
                      return (
                          <NavDropdown key={`${costYear.id}`} eventKey={`${costYear.id}`} title={costYear.year}
                                       id="nav-dropdown-within-tab">
                            {costYear.costMonths.map((costMonth) => {
                              return (
                                  <MenuItem key={`${costYear.id}.${costMonth.id}`}
                                            eventKey={`${costYear.id}.${costMonth.id}`}>
                                    { `${costMonth.monthInYear}/${costMonth.year}` }
                                  </MenuItem>
                              )
                            })}
                          </NavDropdown>
                      );
                    }
                  })}
                </Nav>
              </Row>
            </Tab.Container>
          </div>
      )
    }
    return null
  }

  getDefaultActiveKey(costYears) {
    for (let year of costYears) {
      if (year.costMonths && year.costMonths.length) {
        return `${year.id}.${year.costMonths[0].id}`;
      }
    }
    return null;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    costMonthKey: state.selectedCostMonthId,
    costYears: state.costYears
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    apiGetUsersMeCostYearsSortedByYearDesc: () => dispatch(apiGetUsersMeCostYearsSortedByYearDesc()),
    apiPostCostMonth: (monthInYear, year) => dispatch(apiPostCostMonth(monthInYear, year)),
    selectCashFlowCostMonth: (costMonthId) => dispatch(selectCashFlowCostMonth(costMonthId))
  }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CashFlowView);
