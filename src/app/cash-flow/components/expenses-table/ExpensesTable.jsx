import React, {Component} from 'react';
import Databoard from '../../../shared/components/databoard/Databoard';

import {connect} from 'react-redux';
import {
  apiGetUsersMeRecipients,
  apiGetExpenses,
  editExpensesTableCell,
  resetExpensesTableCell
} from '../../actions/actions';

class ExpensesTable extends Component {

  componentDidMount() {
    this.props.apiGetUsersMeRecipients();
  }

  render() {
    const {
        costMonthKey,
        apiGetExpenses,
        expenses,
        editExpensesTableCell,
        resetExpensesTableCell,
        editedExpensesTableCells,
        recipients,
        user
    } = this.props;

    const onTableStateChange = (pageNumber, pageSize, sort, filtersState) => {
      resetExpensesTableCell();
      apiGetExpenses(costMonthKey.split('.')[1], pageNumber - 1, pageSize, sort, filtersState);
    };

    const config = {
      title: "Expenses",
      noTitle: false,
      noPagination: false,
      noFilters: false,
      maxNumberOfPages: 5,
      noSearch: true,
      numberOfRecordsOnPage: 50,
      showFirstPageSelector: true,
      showLastPageSelector: true,
      showPreviousPageSelector: true,
      showNextPageSelector: true,
      numberOfRecordsOnPageValues: [5, 10, 25, 50],
      sort: {
        columnIndex: 0,
        path: "date",
        direction: "asc"
      },
      onTableStateChange: onTableStateChange,
      onRowClick: (event, row, rowIndex) => {
        console.log('event: ', event, ' row: ', row, ' rowIndex:', rowIndex);
      },
      columns: [
        {
          title: "Day",
          path: "date",
          noSort: false,
          classNames: "pointer-cursor",
          noTitle: false,
          format: {
            type: "date",
            format: "D"
          },
          filter: {
            type: "number"
          },
          onCellClick: (event, row, cellValue, rowIndex, colIndex) => {
            editExpensesTableCell(rowIndex, colIndex);
          }
        },
        {
          title: "Labels",
          noTitle: false,
          noSort: true,
          classNames: "pointer-cursor",
          resolve: (row, rowIndex, colIndex) => {
            let labels = row.labels;

            return labels.map((label) => {
              return (
                  <span key={label.id} className="moneytou-label"
                        style={{backgroundColor: label.backgroundColor, color: label.textColor}}>
                                    {label.name}
                                </span>
              );
            });
          },
          onCellClick: (event, row, cellValue, rowIndex, colIndex) => {
            editExpensesTableCell(rowIndex, colIndex);
          }
        },
        {
          title: "Recipient",
          noTitle: false,
          classNames: "pointer-cursor",
          resolve: (row, rowIndex, colIndex) => {
            let recipientId = row.recipientId;
            let recipient = recipients.find((recipient) => recipient.id === recipientId);

            if (recipient) {
              return recipient.name;
            }
            return recipientId;
          },
          format: {
            type: "text"
          },
          sortPath: "recipient.name",
          filter: {
            type: "text",
            path: "recipient.name"
          },
          onCellClick: (event, row, cellValue, rowIndex, colIndex) => {
            editExpensesTableCell(rowIndex, colIndex);
          }
        },
        {
          title: "Amount",
          path: "amount.value",
          noTitle: false,
          classNames: "pointer-cursor",
          format: {
            type: "amount",
            precision: 0,
            currency: "Kč"
          },
          filter: {
            type: "number"
          },
          onCellClick: (event, row, cellValue, rowIndex, colIndex) => {
            editExpensesTableCell(rowIndex, colIndex);
          }
        },
        {
          title: "Note",
          noSort: true,
          classNames: "pointer-cursor",
          path: "note",
          noTitle: false,
          format: {
            type: "text"
          },
          filter: {
            type: "text"
          },
          onCellClick: (event, row, cellValue, rowIndex, colIndex) => {
            editExpensesTableCell(rowIndex, colIndex);
          }
        }
      ]
    };

    return (
        <div>
          <Databoard config={config} data={expenses.content} totalElements={expenses.totalElements} className="table"/>
          <p>{user.firstName} {user.lastName} {user.username}</p>
        </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    costMonthKey: state.selectedCostMonthId,
    expenses: state.expenses,
    incomes: state.incomes,
    recipients: state.recipients,
    user: state.user,
    editedExpensesTableCells: state.editedExpensesTableCells
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    apiGetUsersMeRecipients: () => dispatch(apiGetUsersMeRecipients()),
    apiGetExpenses: (pageNumber, pageSize, sort, filtersState) => dispatch(apiGetExpenses(pageNumber, pageSize, sort, filtersState)),
    editExpensesTableCell: (rowIndex, cellIndex) => dispatch(editExpensesTableCell(rowIndex, cellIndex)),
    resetExpensesTableCell: () => dispatch(resetExpensesTableCell())
  }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExpensesTable);
