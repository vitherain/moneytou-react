export default class ExpenseWizardProgress {

  static AMOUNT = 'AMOUNT';
  static DATE = 'DATE';
  static LABELS = 'LABELS';
  static RECIPIENT = 'RECIPIENT';
  static NOTE = 'NOTE';
}
