import ExpenseWizardProgress from "./ExpenseWizardProgress";

export default class ExpenseUtil {

  static getNextStep(currentStep) {
    switch (currentStep) {
      case ExpenseWizardProgress.AMOUNT:
            return ExpenseWizardProgress.DATE;
      case ExpenseWizardProgress.DATE:
            return ExpenseWizardProgress.LABELS;
      case ExpenseWizardProgress.LABELS:
        return ExpenseWizardProgress.RECIPIENT;
      case ExpenseWizardProgress.RECIPIENT:
        return ExpenseWizardProgress.NOTE;
      case ExpenseWizardProgress.NOTE:
        return null;
    }
  }

  static getPreviousStep(currentStep) {
    switch (currentStep) {
      case ExpenseWizardProgress.AMOUNT:
            return null;
      case ExpenseWizardProgress.DATE:
            return ExpenseWizardProgress.AMOUNT;
      case ExpenseWizardProgress.LABELS:
        return ExpenseWizardProgress.DATE;
      case ExpenseWizardProgress.RECIPIENT:
        return ExpenseWizardProgress.LABELS;
      case ExpenseWizardProgress.NOTE:
        return ExpenseWizardProgress.RECIPIENT;
    }
  }

}
