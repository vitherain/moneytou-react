export const getLabelStyle = (label) => {
  return {
      color: label.textColor ? label.textColor : 'black',
      backgroundColor: (label.backgroundColor ? label.backgroundColor : 'white')
  };
};

export const getFaIconName = (label) => {
  return label.faIcon ? label.faIcon : 'question';
};
