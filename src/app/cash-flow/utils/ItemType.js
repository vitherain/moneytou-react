export default class ItemType {

  static EXPENSE = 'EXPENSE';
  static INCOME = 'INCOME';
}
