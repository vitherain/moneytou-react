import * as C from '../../shared/config/constants';
import {getAuthToken} from '../../shared/utils/authTokenHolder';
import {mapToRequestParams} from '../../shared/utils/requestParamsMapper';

import Axios from 'axios';

export const EDIT_EXPENSES_TABLE_CELL = 'EDIT_EXPENSES_TABLE_CELL';
export const RESET_EXPENSES_TABLE_CELL = 'RESET_EXPENSES_TABLE_CELL';

export const API_GET_EXPENSES = 'API_GET_EXPENSES';
export const API_POST_EXPENSES = 'API_POST_EXPENSES';

export const SELECT_CASH_FLOW_COST_MONTH = 'SELECT_CASH_FLOW_COST_MONTH';
export const API_GET_COST_YEARS_SORTED_BY_YEAR_DESC = 'API_GET_COST_YEARS_SORTED_BY_YEAR_DESC';

export const API_GET_RECIPIENTS = 'API_GET_RECIPIENTS';
export const API_POST_RECIPIENT = 'API_POST_RECIPIENT';

export const API_GET_LABELS = 'API_GET_LABELS';
export const API_POST_LABEL = 'API_POST_LABEL';


export const API_POST_COST_MONTH = 'API_POST_COST_MONTH';

export const editExpensesTableCell = (rowIndex, colIndex) => {
  return (dispatch) => {
    return dispatch({
      type: EDIT_EXPENSES_TABLE_CELL,
      payload: new Promise(resolve => ({ rowIndex, colIndex }))
    });
  };
};

export const resetExpensesTableCell = () => {
  return (dispatch) => {
    return dispatch({
      type: RESET_EXPENSES_TABLE_CELL,
      payload: new Promise()
    });
  };
};

export const apiGetUsersMeCostYearsSortedByYearDesc = () => {
  return (dispatch) => {
    let reqConfig = {
      method: C.GET_METHOD,
      url: `${process.env.API_URL}/api/v1/users/me/cost-years`,
      headers: {
        [C.AUTHORIZATION_HEADER_NAME]: getAuthToken()
      }
    };
    reqConfig = mapToRequestParams(
        reqConfig,
        {
          sort: {
            path: 'year',
            direction: 'desc'
          }
        }
    );

    return dispatch({
      type: API_GET_COST_YEARS_SORTED_BY_YEAR_DESC,
      payload: new Promise((resolve, reject) => {
        Axios(reqConfig).then(
            (response) => resolve(response.data),
            (error) => reject(error.response)
        );
      })
    });
  };
};

export const apiPostCostMonth = (monthInYear, year) => {
  return (dispatch) => {
    const reqConfig = {
      method: C.POST_METHOD,
      url: `${process.env.API_URL}/api/v1/users/me/cost-months`,
      headers: {
        [C.AUTHORIZATION_HEADER_NAME]: getAuthToken()
      },
      data: {
        monthInYear,
        year
      }
    };

    return dispatch({
      type: API_POST_COST_MONTH,
      payload: new Promise((resolve, reject) => {
        Axios(reqConfig).then(
            (response) => resolve(response.data),
            (error) => reject(error.response)
        );
      })
    });
  };
};

export const selectCashFlowCostMonth = (costMonthId) => {
  return (dispatch) => {
    return dispatch({
      type: SELECT_CASH_FLOW_COST_MONTH,
      payload: new Promise(resolve => ({ selectedCostMonthId: costMonthId }))
    });
  };
};

export const apiGetExpenses = (costMonthId, pageNumber, pageSize, sort, filtersState) => {
  return (dispatch) => {
    let reqConfig = {
      method: C.GET_METHOD,
      url: `${process.env.API_URL}/api/v1/users/me/cost-months/${costMonthId}/expenses`,
      headers: {
        [C.AUTHORIZATION_HEADER_NAME]: getAuthToken()
      }
    };
    reqConfig = mapToRequestParams(
        reqConfig,
        {
          pageNumber,
          pageSize,
          sort,
          filtersState
        }
    );

    return dispatch({
      type: API_GET_EXPENSES,
      payload: new Promise((resolve, reject) => {
        Axios(reqConfig).then(
            (response) => resolve(response.data),
            (error) => reject(error.response)
        );
      })
    });
  };
};

export const apiGetUsersMeRecipients = () => {
  return (dispatch) => {
    const reqConfig = {
        method: C.GET_METHOD,
        url: `${process.env.API_URL}/api/v1/users/me/recipients`,
        headers: {
            [C.AUTHORIZATION_HEADER_NAME]: getAuthToken()
        }
    };

    return dispatch({
        type: API_GET_RECIPIENTS,
        payload: new Promise((resolve, reject) => {
            Axios(reqConfig).then(
                (response) => resolve(response.data),
                (error) => reject(error.response)
            );
        })
    });
  };
};

export const apiGetUsersMeLabels = () => {
  return (dispatch) => {
    const reqConfig = {
        method: C.GET_METHOD,
        url: `${process.env.API_URL}/api/v1/users/me/labels`,
        headers: {
            [C.AUTHORIZATION_HEADER_NAME]: getAuthToken()
        }
    };

    return dispatch({
        type: API_GET_LABELS,
        payload: new Promise((resolve, reject) => {
            Axios(reqConfig).then(
                (response) => resolve(response.data),
                (error) => reject(error.response)
            );
        })
    });
  };
};

export const apiPostUsersMeLabel = (newLabel) => {
  return (dispatch) => {
      const reqConfig = {
          method: C.POST_METHOD,
          url: `${process.env.API_URL}/api/v1/users/me/labels`,
          headers: {
              [C.AUTHORIZATION_HEADER_NAME]: getAuthToken()
          },
          data: newLabel
      };

      return dispatch({
          type: API_POST_LABEL,
          payload: new Promise((resolve, reject) => {
              Axios(reqConfig).then(
                  (response) => resolve(response.data),
                  (error) => reject(error.response)
              );
          })
      });
  };
};

export const apiPostUsersMeRecipient = (newRecipient) => {
  return (dispatch) => {
      const reqConfig = {
          method: C.POST_METHOD,
          url: `${process.env.API_URL}/api/v1/users/me/recipients`,
          headers: {
              [C.AUTHORIZATION_HEADER_NAME]: getAuthToken()
          },
          data: newRecipient
      };

      return dispatch({
          type: API_POST_RECIPIENT,
          payload: new Promise((resolve, reject) => {
              Axios(reqConfig).then(
                  (response) => resolve(response.data),
                  (error) => reject(error.response)
              );
          })
      });
  };
};

export const apiPostUsersMeExpense = (newExpense) => {
  return (dispatch) => {
      const datePattern = /(\d{1,4})-(\d{1,4})-(\d{1,4})/;
      const expense = {
          date: newExpense.date ? new Date(Date.UTC(
              datePattern.exec(newExpense.date)[1],
              parseInt(datePattern.exec(newExpense.date)[2]) - 1,
              datePattern.exec(newExpense.date)[3]
          ))
          : null,
          amount: { ...newExpense.amount, currency: 'CZK' },
          recipientId: newExpense.recipient ? newExpense.recipient.id : null,
          labels: newExpense.labels ? newExpense.labels.map(l => l.id) : null,
          note: newExpense.note
      };

      const reqConfig = {
          method: C.POST_METHOD,
          url: `${process.env.API_URL}/api/v1/users/me/expenses`,
          headers: {
              [C.AUTHORIZATION_HEADER_NAME]: getAuthToken()
          },
          data: expense
      };

      return dispatch({
          type: API_POST_EXPENSES,
          payload: new Promise((resolve, reject) => {
              Axios(reqConfig).then(
                  (response) => resolve(response.data),
                  (error) => reject(error.response)
              );
          })
      });
  };
};
