import * as C from '../../shared/config/constants';
import {getAuthToken} from '../../shared/utils/authTokenHolder';

import Axios from 'axios';
import {browserHistory} from "react-router";

export const API_POST_LOGIN = 'API_POST_LOGIN';
export const API_POST_LOGOUT = 'API_POST_LOGOUT';

export const apiPostLoginWithRedirectToCashFlow = (values) => {
  return (dispatch) => {
    const reqConfig = {
      method: C.POST_METHOD,
      url: `${process.env.API_URL}/api/v1/auth`,
      headers: {'X-Auth-Username': values.username, 'X-Auth-Password': values.password}
    };

    return dispatch({
      type: API_POST_LOGIN,
      payload: new Promise((resolve, reject) => {
        Axios(reqConfig).then(
            (response) => {
                localStorage.setItem(C.LOCAL_STORAGE_AUTH_TOKEN_KEY, response.headers[C.X_AUTH_HEADER_NAME.toLowerCase()]);
                localStorage.setItem(C.LOCAL_STORAGE_CURRENT_USER_KEY, JSON.stringify(response.data));
                resolve(response.data);
            },
            (error) => reject(error.response)
        );
      })
    }).then(() => browserHistory.push('/cash-flow'));
  };
};

export const apiPostLogout = () => {
  return (dispatch) => {
    const reqConfig = {
      method: C.POST_METHOD,
      url: `${process.env.API_URL}/api/v1/logout`,
      headers: {
        [C.AUTHORIZATION_HEADER_NAME]: getAuthToken()
      }
    };

    return dispatch({
      type: API_POST_LOGOUT,
      payload: new Promise((resolve, reject) => {
        Axios(reqConfig).then(
            () => {
                localStorage.removeItem(C.LOCAL_STORAGE_AUTH_TOKEN_KEY);
                localStorage.removeItem(C.LOCAL_STORAGE_CURRENT_USER_KEY);
                resolve();
            },
            (error) => reject(error.response)
        );
      })
    });
  };
};
