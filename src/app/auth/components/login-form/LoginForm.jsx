import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import { Button } from 'reactstrap';

import {connect} from 'react-redux';
import {apiPostLoginWithRedirectToCashFlow} from '../../actions/actions';

class LoginForm extends Component {

  render() {
    const {handleSubmit, apiPostLoginWithRedirectToCashFlow} = this.props;

    return (
        <form onSubmit={ handleSubmit(apiPostLoginWithRedirectToCashFlow) }>
          <div className="form-group row">
            <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
            <div className="col-sm-10">
              <Field name="username" type="email" className="form-control" component="input" placeholder="Email"/>
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="password" className="col-sm-2 col-form-label">Password</label>
            <div className="col-sm-10">
              <Field name="password" type="password" className="form-control" component="input" placeholder="Password"/>
            </div>
          </div>
          <div className="form-group row">
            <div className="offset-sm-2 col-sm-10">
              <Button type="submit" color="primary" className="pointer-cursor">Sign in</Button>
            </div>
          </div>
        </form>
    )
  }
}

LoginForm = reduxForm({
  form: 'login'
})(LoginForm);

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    apiPostLoginWithRedirectToCashFlow: (values) => dispatch(apiPostLoginWithRedirectToCashFlow(values))
  }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginForm);
