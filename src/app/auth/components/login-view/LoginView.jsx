import React, {Component} from 'react';
import LoginForm from '../login-form/LoginForm';

class Login extends Component {

  render() {
    return (
        <div>
          <h1>Login page</h1>
          <LoginForm/>
        </div>
    )
  }
}

export default Login;
