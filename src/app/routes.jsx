import React from 'react';
import {IndexRoute, Route} from "react-router";
import NotFoundView from "./core/components/not-found-view/NotFoundView";
import LoginView from "./auth/components/login-view/LoginView";
import NewCashFlowItemView from "./cash-flow/components/new-cash-flow-item-view/NewCashFlowItemView";
import HomeView from "./core/components/home-view/HomeView";
import App from "./core/components/app/App";

const routes = (
  <Route path="/" component={App}>
    <IndexRoute component={HomeView}/>
    <Route path="cash-flow" component={NewCashFlowItemView}/>
    <Route path="login" component={LoginView}/>
    <Route path="*" component={NotFoundView}/>
  </Route>
);

export default routes;
