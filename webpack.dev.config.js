const path = require('path');
const webpack = require('webpack');
const config = require('./webpack.config.js');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const port = 3000;

const jsLoaderIndex = config.module.rules.findIndex((loader) => {
  if (loader.use) {
    return loader.use.loader === 'babel-loader';
  }
  return false;
});

config.module.rules.splice(jsLoaderIndex, 1, {
  test: /\.js|.jsx?$/,
  exclude: /node_modules/,
  use: [
    {
      loader: 'react-hot-loader/webpack'
    },
    {
      loader: 'babel-loader',
      options: {
        presets: ['env', ["es2015", { "modules": false }], 'react', 'stage-2'],
        plugins: ['transform-class-properties', 'transform-object-rest-spread']
      }
    }
  ],
});

config.plugins.push(new BundleAnalyzerPlugin());

config.entry.unshift(
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:' + port,
    'webpack/hot/only-dev-server'
);

config.devServer = {
  contentBase: path.join(__dirname, 'src', 'static'),
  compress: true,
  port: port,
  historyApiFallback: {
    index: 'index.html'
  }
};

config.devtool = 'source-map';

module.exports = config;
