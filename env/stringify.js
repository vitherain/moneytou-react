function stringifyFn(envConfig) {
  for (const key in envConfig) {
    if (envConfig.hasOwnProperty(key)) {
      envConfig[key] = JSON.stringify(envConfig[key]);
    }
  }
  return envConfig;
}

module.exports = stringifyFn;